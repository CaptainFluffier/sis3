<?php
class News_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_news(){
		$query = $this->db->get('user_login');
		return $query->result_array();
	}
	public function get_reports(){
		$sql = $this->db->get('reports');
		return $sql->result_array();
	}
	public function get_all_games(){
		$query = $this->db->get('igre');
                return $query->result_array();
	}
	public function filter_news_title($title){
		$sql = $this->db->query("SELECT nickname FROM user_login u WHERE nickname LIKE '$title%'") -> result_array(); 
		return $sql;
	}
	public function filter_news_game($box){
		$query = $this->db->query(" SELECT nickname FROM user_login u inner join uigra ui on (u.id = ui.idu) inner join igre i on(i.idi = ui.idi) WHERE naslov LIKE '$box%'") -> result_array();
                return $query;
	}
	public function filter_news_both($title,$box){
                $query = $this->db->query(" SELECT nickname FROM user_login u inner join uigra ui on (u.id = ui.idu) inner join igre i on(i.idi = ui.idi) WHERE naslov LIKE '$box%' AND nickname LIKE '$title%'") -> result_array();
                return $query;
        }
	public function get_news_where($slug){
		$query = $this->db->get_where('user_login', array('nickname' => $slug));
		return $query->row_array();
	}
	public function where($slug){
		$query = $this->db->get_where('user_login', array('user_name' => $slug)) ->row();
                return $query;
	}
	public function to_json(){
		$sql = $this->db->query(" SELECT nickname,naslov FROM user_login u inner join uigra ui on (u.id = ui.idu) inner join igre i on(i.idi = ui.idi) GROUP BY nickname,naslov") -> result();
		echo json_encode($sql);	
		return $sql;
	}

	public function set_news(){
		$data = array(
			'naslov' => $this->input->post('title'));
		return $this->db->insert('igre', $data);			
	}

	public function getPending($data){
	$query = $this->db->query("SELECT user2 FROM friends WHERE user1 = '$data[username]' AND accepted = '0' ") -> result_array();
        return $query;
	}
	public function getFriends($data){
	$query = $this->db->query("SELECT user2 FROM friends WHERE (user1 = '$data[username]' OR user2 = '$data[username]') AND accepted = '1' ") -> result_array();
	return $query;
	}

	 public function uigre_add($data){
		$idu = $this->db->query("SELECT id FROM user_login WHERE '$data[username]' = user_name") -> row();
		$idi = $this->db->query("SELECT idi FROM igre WHERE '$data[naslov]' = naslov") -> row();
		$podatki = array (
			'idu' =>  $idu -> id,
			'idi' => $idi -> idi);
		return $this->db->insert('uigra',$podatki);
        }
	public function send_friend($data){
	$query = $this->db->query("SELECT user1,user2 FROM friends") -> result();
	foreach ($query as & $qr) {
	if(strcmp($qr->user1,$data['prejemnik']) == 0 && strcmp($qr->user2,$data['sender']) == 0) return 0;
	if(strcmp($qr->user2,$data['prejemnik']) == 0 && strcmp($qr->user1,$data['sender']) == 0) return 0;
	if(!strcmp($data['sender'],$data['prejemnik'])) return 0;
}
	$podatki = array (
	'user1' => $data['prejemnik'],
	'user2' => $data['sender'],
	'accepted' => '0');
	return $this->db->insert('friends',$podatki);
	}
public function report_user($data){
$podatki = array (
'user1' => $data['username'],
'user2' => $data['reported'],
'msg' => $data['msg']);
return $this->db->insert('reports',$podatki);
}

public function update_news($id){
                $data = array(
                        'user_email' => $this->input->post('email'),
                        'nickname' => $this->input->post('nickname'),
                        'user_password' => $this->input->post('password'));

		$this->db->where('user_name', $id);
                return $this->db->update('user_login', $data);
        }
public function acceptFriend($data){
$dataN = array(
	 'user1' => $data['prejemnik'],
        'user2' => $data['sender'],
        'accepted' => '1');
$this->db->where('user1', $data['prejemnik']);
$this ->db -> where('user2', $data['sender']);

return $this->db->update('friends',$dataN);
}
public function denyFriend($data){
$this->db->where('user1', $data['prejemnik']);
$this ->db -> where('user2', $data['sender']);
return $this->db->delete('friends');
}

public function delete_news($slug){
	echo 'deleted '; echo $slug;
	$this->db->where('slug', $slug);
	return $this->db->delete('books');
}

public function get_games($slug){
 $query = $this->db->query(" SELECT naslov FROM user_login u inner join uigra ui on (u.id = ui.idu) inner join igre i on(i.idi = ui.idi) WHERE nickname = '$slug' GROUP BY naslov") -> result_array();
 return $query;
}
public function areFriends($data){
$query = $this->db->query(" SELECT accepted FROM friends WHERE (user1 = '$data[username]' OR user1 = '$data[sender]' ) AND (user2 = '$data[username]' OR user2 = '$data[sender]')") -> result_array();
return $query;
}
public function removeFriend($data){
$this->db->where('user1',$data['prejemnik']);
$this->db->where('user2',$data['sender']);
$this->db->delete('friends');

if($this->db->affected_rows()>0)
return true;
$this->db->where('user1',$data['sender']);
$this->db->where('user2',$data['prejemnik']);
$this->db->delete('friends');
if($this->db->affected_rows()>0)
return true;
return false;

}

}
