<?php
class News extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('news_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->helper('form');
                $this->load->library('form_validation');
	}

	public function json(){
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
				$tmp = $this->news_model->to_json();
		}else {
			$data['message_display'] = 'Sign in to use json!';
		       $this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login_form', $data);
     			   $this->load->view('templates/footer', $data);
			}
	}

	public function add(){
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
	if(isset($this->session->userdata['admin']))
	$data['username'] = $this->session->userdata['admin']['username'];
	else
	$data['username'] = $this->session->userdata['logged_in']['username'];
	$text = $this->uri->segment(3);
	$data['naslov'] = str_replace('%20', ' ', substr($text, 0, strlen($text)));
	$this->news_model->uigre_add($data);
	redirect('news/games');
	echo "Game added";
		}
		else{
		$data['message_display'] = 'Sign in to add games to profile';
                       $this->load->view('templates/header', $data);
                        $this->load->view('user_authentication/login_form', $data);
                           $this->load->view('templates/footer', $data);
		}
	}
	public function friend(){
		    if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
                $data['prejemnik'] = $this->uri->segment(3);
		  if(isset($this->session->userdata['admin']))
       		 $data['sender'] = $this->session->userdata['admin']['username'];
      		  else
       		 $data['sender'] = $this->session->userdata['logged_in']['username'];
		if(strcmp($this->news_model->send_friend($data),'0'))
		$this->load->view('templates/friend');
		else
		$this->load->view('templates/friend_failed');
		$this->load->view('templates/header');
                          if(isset($this->session->userdata['admin']))
                        $this->load->view('templates/admin');
                  $this->load->view('templates/footer');

        }
        else {
                 $data['message_display'] = 'Sign in to use friend feature';
                 $this->load->view('templates/header', $data);
                 $this->load->view('user_authentication/login_form', $data);
                 $this->load->view('templates/footer', $data);
        }

	}	

	public function games(){
	if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
		$data['news'] = $this->news_model->get_all_games();
                $data['title'] = "Games";
		$this->load->view('templates/header', $data);
			  if(isset($this->session->userdata['admin']))
                        $this->load->view('templates/admin');
		   $this->load->view('news/games', $data);
		  $this->load->view('templates/footer');
	}
	else {
		 $data['message_display'] = 'Sign in to add games to profile!';
                 $this->load->view('templates/header', $data);
                 $this->load->view('user_authentication/login_form', $data);
                 $this->load->view('templates/footer', $data);
	}
}

	public function admin_reports(){
	if(isset($this->session->userdata['admin'])){
		$data['reports'] = $this->news_model->get_reports();
                $data['title'] = "All reports";
		 $this->load->view('templates/header');
                $this->load->view('templates/admin');
                $this->load->view('news/reports', $data);
                 $this->load->view('templates/footer', $data);
                }
                else{
                         $data['message_display'] = 'Sign in as admin to view admin page!';
                         $this->load->view('templates/header');
                         $this->load->view('user_authentication/login_form', $data);
                         $this->load->view('templates/footer');
                }
	}

	public function filter(){
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
			 $titles = $this ->input -> post("filter_title");
                         $box = $this -> input -> post("filter_game");
			if(empty($titles)){
				$data['news']=$this->news_model->filter_news_game($box);
			}
			else if(empty($box)){
				$data['news']=$this->news_model->filter_news_title($titles);
			}
			else{
				$data['news']=$this->news_model->filter_news_both($titles, $box);
			}
				   $this->load->view('templates/header', $data);
					 if(isset($this->session->userdata['admin']))
                $this->load->view('templates/admin');
			                $this->load->view('news/index', $data);
               				 $this->load->view('templates/footer', $data);
		}else {
                        $data['message_display'] = 'Sign in to use filter!';
                       $this->load->view('templates/header', $data);
                        $this->load->view('user_authentication/login_form', $data);
                           $this->load->view('templates/footer', $data);
                        }
	}

	public function delete(){
		if(isset($this->session->userdata['admin'])){
			$slug = $this->uri->segment(3);
			$this->news_model->delete_news($slug);
			redirect('news','refresh');

		}
		else{
	   		 $data['message_display'] = 'Sign in as admin to view admin page!';
           		 $this->load->view('templates/header');
           		 $this->load->view('user_authentication/login_form', $data);
           		 $this->load->view('templates/footer');
		}
	}
	

	public function connector(){
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
                          if(isset($this->session->userdata['admin']))
                         $data['username'] = $this->session->userdata['admin']['username'];
                         else
                         $data['username'] = $this->session->userdata['logged_in']['username'];
		$dt = $this -> news_model -> where($data['username']);
		$data['password'] = $dt -> user_password;
		$data['id'] = $dt -> id;
		$data['nickname'] = $dt -> nickname;
		$data['admin'] = $dt -> user_admin;
		$data['email'] = $dt -> user_email;
		$this->load->view('templates/header', $data);
	        $this->load->view('news/connector', $data);
       		$this->load->view('templates/footer', $data);
		}else{
			  $data['message_display'] = 'Sign in to view profile!';
                         $this->load->view('templates/header');
                         $this->load->view('user_authentication/login_form', $data);
                         $this->load->view('templates/footer');
		}
	}

	public function report() {
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
                          if(isset($this->session->userdata['admin']))
                         $data['username'] = $this->session->userdata['admin']['username'];
                         else
                         $data['username'] = $this->session->userdata['logged_in']['username'];
                $data['user2'] = $this->uri->segment(3);
                $this->load->view('templates/header', $data);
                $this->load->view('news/report_send', $data);
                $this->load->view('templates/footer', $data);
                }else{
                          $data['message_display'] = 'Sign in to view profile!';
                         $this->load->view('templates/header');
                         $this->load->view('user_authentication/login_form', $data);
                         $this->load->view('templates/footer');
                }

	}

	public function report_send(){
		 if(isset($this->session->userdata['admin']) || isset($this->session->userdata['logged_in'])){
                $this->form_validation->set_rules('msg', 'Msg', 'required');
		$this->form_validation->set_rules('user1', 'User1', 'required');
		$this->form_validation->set_rules('user2', 'User2', 'required');
                $data['title'] = "Report player";
                if($this->form_validation->run() === FALSE){
                        $this->load->view('templates/header', $data);
         
                $this->load->view('templates/header');
                $this->load->view('templates/footer');
                }else{
			if(isset($this->session->userdata['admin']))
                         $data['username'] = $this->session->userdata['admin']['username'];
                         else
                         $data['username'] = $this->session->userdata['logged_in']['username'];
			$data['reported'] = $this->input->post("user2");                       
                        $data['msg'] = $this ->input -> post("msg");
                        $this->news_model->report_user($data);
                        redirect('news/index');
                }  }
                else{
                      $data['message_display'] = 'Sign in to report!';
                         $this->load->view('templates/header');
                         $this->load->view('user_authentication/login_form', $data);
                         $this->load->view('templates/footer');
}                
	}

	public function update(){
            if(isset($this->session->userdata['admin']) || isset($this->session->userdata['logged_in'])){
                $this->form_validation->set_rules('password', 'Password', 'required');
                $this->form_validation->set_rules('username', 'Username', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('nickname', 'Nickname', 'required');
                $data['title'] = "Edit profile";
                if($this->form_validation->run() === FALSE){
                        $this->load->view('templates/header', $data);
	
                $this->load->view('templates/header');
                $this->load->view('templates/footer');
                }else{
			$id = $this ->input -> post("username");
                        $this->news_model->update_news($id);
			redirect('news/connector');
                }  }
		else{
                      $data['message_display'] = 'Sign in to edit profile!';
                         $this->load->view('templates/header');
                         $this->load->view('user_authentication/login_form', $data);
                         $this->load->view('templates/footer');
                }

	}
	public function friendPage(){
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
			  if(isset($this->session->userdata['admin']))
       			 $data['username'] = $this->session->userdata['admin']['username'];
       			 else
       			 $data['username'] = $this->session->userdata['logged_in']['username'];
			$data['news'] = $this->news_model->getFriends($data);
               		 $data['title'] = "Friend list";
			$this->load->view('templates/header', $data);
			$this->load->view('news/pending');
			$this->load->view('news/friendPage', $data);
               		 if(isset($this->session->userdata['admin']))
               		 $this->load->view('templates/admin');
                $this->load->view('templates/footer', $data);
		}
  else{
                $data['message_display'] = 'Sign in to view friend page';
                       $this->load->view('templates/header', $data);
                        $this->load->view('user_authentication/login_form', $data);
                           $this->load->view('templates/footer', $data);
                }

		
	}

        public function pending(){
                if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
                          if(isset($this->session->userdata['admin']))
                         $data['username'] = $this->session->userdata['admin']['username'];
                         else
                         $data['username'] = $this->session->userdata['logged_in']['username'];
                        $data['news'] = $this->news_model->getPending($data);
                         $data['title'] = "Friend list";
                        $this->load->view('templates/header', $data);
                        $this->load->view('news/pending');
                        $this->load->view('news/requestPage', $data);
                         if(isset($this->session->userdata['admin']))
                         $this->load->view('templates/admin');
                $this->load->view('templates/footer', $data);
                }
  else{
                $data['message_display'] = 'Sign in to view friend page';
                       $this->load->view('templates/header', $data);
                        $this->load->view('user_authentication/login_form', $data);
                           $this->load->view('templates/footer', $data);
                }

                
        }
	public function accept(){
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
                $data['sender'] = $this->uri->segment(3);
                  if(isset($this->session->userdata['admin']))
                 $data['prejemnik'] = $this->session->userdata['admin']['username'];
                  else
                 $data['prejemnik'] = $this->session->userdata['logged_in']['username'];
		if($this->news_model->acceptFriend($data))
		print_r('Friend request accepted');
		else
		print_r('Error');
                $this->load->view('templates/header');
                          if(isset($this->session->userdata['admin']))
                        $this->load->view('templates/admin');
                  $this->load->view('templates/footer');

        }
        else {
                 $data['message_display'] = 'Sign in to use friend feature';
                 $this->load->view('templates/header', $data);
                 $this->load->view('user_authentication/login_form', $data);
                 $this->load->view('templates/footer', $data);
        }
	
}
	public function deny(){
	  if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
                $data['sender'] = $this->uri->segment(3);
                  if(isset($this->session->userdata['admin']))
                 $data['prejemnik'] = $this->session->userdata['admin']['username'];
                  else
                 $data['prejemnik'] = $this->session->userdata['logged_in']['username'];
                if($this->news_model->denyFriend($data))
                print_r('Friend request denied');
                else
                print_r('Error');
                $this->load->view('templates/header');
                          if(isset($this->session->userdata['admin']))
                        $this->load->view('templates/admin');
                  $this->load->view('templates/footer');

        }
        else {
                 $data['message_display'] = 'Sign in to use friend feature';
                 $this->load->view('templates/header', $data);
                 $this->load->view('user_authentication/login_form', $data);
                 $this->load->view('templates/footer', $data);
        }

	} 	

	public function create(){
	//	$this->load->helper('form');
	//	$this->load->library('form_validation');
		if(isset($this->session->userdata['admin'])){ 
		$this->form_validation->set_rules('title', 'Title', 'required');
		$data['title'] = "Add games";

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('news/create');
        	$this->load->view('templates/footer');
		}else{
			$this->news_model->set_news();
			$this->load->view('news/success');
			$this->load->view('templates/header');
			$this->load->view('templates/admin');
			$this->load->view('templates/footer');
		}}else{
		      $data['message_display'] = 'Sign in as admin to add games!';
                         $this->load->view('templates/header');
                         $this->load->view('user_authentication/login_form', $data);                
		         $this->load->view('templates/footer');
		}
	}
	public function removeFriend(){
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
                $data['sender'] = $this->uri->segment(3);
                  if(isset($this->session->userdata['admin']))
                 $data['prejemnik'] = $this->session->userdata['admin']['username'];
                  else
                 $data['prejemnik'] = $this->session->userdata['logged_in']['username'];
                if($this->news_model->removeFriend($data))
                print_r('Friend removed');
                else
                print_r('Error');
                $this->load->view('templates/header');
                          if(isset($this->session->userdata['admin']))
                        $this->load->view('templates/admin');
                  $this->load->view('templates/footer');

        }
        else {
                 $data['message_display'] = 'Sign in to use friend feature';
                 $this->load->view('templates/header', $data);
                 $this->load->view('user_authentication/login_form', $data);
                 $this->load->view('templates/footer', $data);
        }

	}
	public function view($slug)
	{
		if(isset($this->session->userdata['logged_in']) || isset($this->session->userdata['admin'])){
		if(isset($this->session->userdata['admin']))
                         $data['username'] = $this->session->userdata['admin']['username'];
                         else
                         $data['username'] = $this->session->userdata['logged_in']['username'];
		$data['sender'] = $this->uri->segment(2);
		$data['news_item'] = $this->news_model->get_news_where($slug);
		$data['title'] = "Profile view";
		$data['friend'] = $this->news_model->areFriends($data);
		$data['games'] = $this->news_model->get_games($slug);
		$this->load->view('templates/header', $data);
		if(isset($this->session->userdata['admin']))
		$this->load->view('templates/admin');
	        $this->load->view('news/view', $data);
		$this->load->view('templates/footer', $data);
	}else{
$data['message_display'] = 'Sign in to view users';
                 $this->load->view('templates/header', $data);
                 $this->load->view('user_authentication/login_form', $data);
                 $this->load->view('templates/footer', $data);
	}}


    public function index()
	{
		$data['news'] = $this->news_model->get_news();
		$data['title'] = "Players";
		$this->load->view('templates/header', $data);
		 if(isset($this->session->userdata['admin']))
                $this->load->view('templates/admin');
	        $this->load->view('news/index', $data);
       		 $this->load->view('templates/footer', $data);
	}

}
