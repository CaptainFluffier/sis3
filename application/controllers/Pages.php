<?php
class Pages extends CI_Controller {

        public function view($lang = '', $page = 'home')
		{
			$this->load->helper('url_helper');
			$this->load->library('session');


	        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
	        {
	                // Whoops, we don't have a page for that!
	                show_404();
	        }

	        $data['title'] = ucfirst($page); // Capitalize the first letter

	        $this->load->view('templates/header', $data);
		 if(isset($this->session->userdata['admin']))
                $this->load->view('templates/admin');
		$this->lang->load('content', $lang=='' ? 'english' : $lang);
		$data['msg'] = $this->lang->line('msg');
	        $this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
		}

}
